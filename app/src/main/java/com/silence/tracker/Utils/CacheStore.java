package com.silence.tracker.Utils;

/**
 * Created by Houssem on 10/07/2015.
 */

import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;
import java.util.HashMap;

public class CacheStore {
    private static CacheStore INSTANCE = null;
    private HashMap cacheMap;
    private static final String cacheDir = "/Android/data/com.example.ttttt/cache/";
    private static final String CACHE_FILENAME = ".cache";

    @SuppressWarnings("unchecked")
    public CacheStore() {
        cacheMap = new HashMap();
        File fullCacheDir = new File(Environment.getExternalStorageDirectory().toString(), cacheDir);
        if (!fullCacheDir.exists()) {
            Log.i("CACHE", "Directory doesn't exist");
            cleanCacheStart();
            return;
        }
        try {
            ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(fullCacheDir.toString(), CACHE_FILENAME))));
            cacheMap = (HashMap) is.readObject();
            is.close();
        } catch (StreamCorruptedException e) {
            Log.i("CACHE", "Corrupted stream");
            cleanCacheStart();
        } catch (FileNotFoundException e) {
            Log.i("CACHE", "File not found");
            cleanCacheStart();
        } catch (IOException e) {
            Log.i("CACHE", "Input/Output error");
            cleanCacheStart();
        } catch (ClassNotFoundException e) {
            Log.i("CACHE", "Class not found");
            cleanCacheStart();
        }
    }

    public void cleanCacheStart() {
        cacheMap = new HashMap();
        File fullCacheDir = new File(Environment.getExternalStorageDirectory().toString(), cacheDir);
        fullCacheDir.mkdirs();
        File noMedia = new File(fullCacheDir.toString(), ".nomedia");
        try {
            noMedia.createNewFile();
            Log.i("CACHE", "Cache created");
        } catch (IOException e) {
            Log.i("CACHE", "Couldn't create .nomedia file");
            e.printStackTrace();
        }
    }

    private synchronized static void createInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CacheStore();
        }
    }

    public static CacheStore getInstance() {
        if (INSTANCE == null) createInstance();
        return INSTANCE;
    }

    public void saveCacheFile(String fileLocalName) {
        File fullCacheDir = new File(Environment.getExternalStorageDirectory().toString(), cacheDir);
        File fileUri = new File(fullCacheDir.toString(), fileLocalName);
        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(fileUri);
            outStream.write(fileLocalName.getBytes());
            outStream.flush();
            outStream.close();
        } catch (FileNotFoundException e) {
            //Log.i("CACHE", "Error: File " + cacheUri + " was not found!");
            e.printStackTrace();
        } catch (IOException e) {
            // Log.i("CACHE", "Error: File could not be stuffed!");
            e.printStackTrace();
        }
    }
    public String getCacheFile(String cacheUri) {
        if (!cacheMap.containsKey(cacheUri)) return null;
        String fileLocalName = (String) cacheMap.get(cacheUri);
        File fullCacheDir = new File(Environment.getExternalStorageDirectory().toString(), cacheDir);
        File fileUri = new File(fullCacheDir.toString(), fileLocalName);
        if (!fileUri.exists()) return null;

        Log.i("CACHE", "File " + cacheUri + " has been found in the Cache");

        return fileLocalName;
    }

}

