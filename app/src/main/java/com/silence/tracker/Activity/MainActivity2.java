package com.silence.tracker.Activity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.silence.tracker.Service.BroadcastReceiverAutostart;
import com.silence.tracker.Service.BroadcastReceiverGPS;
import com.silence.tracker.Service.MyService;
import com.yalantis.contextmenu.R;
import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;
import com.yalantis.contextmenu.lib.MenuObject;
import com.yalantis.contextmenu.lib.MenuParams;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemClickListener;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemLongClickListener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity2 extends ActionBarActivity implements OnMenuItemClickListener,
        OnMenuItemLongClickListener {

    private FragmentManager fragmentManager;
    private DialogFragment mMenuDialogFragment;
    private ToggleButton toggleOneButton;
    private ToggleButton toggleFiveButton;
    private String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Toast.makeText(getApplicationContext(), "AutoStart", Toast.LENGTH_LONG).show();
        fragmentManager = getSupportFragmentManager();
        initToolbar();
        initMenuFragment();
        addFragment(new MainFragment(), true, R.id.container);
    }

    private void initMenuFragment() {
        MenuParams menuParams = new MenuParams();
        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.tool_bar_height));
        menuParams.setMenuObjects(getMenuObjects());
        menuParams.setClosableOutside(false);
        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
    }

    private List<MenuObject> getMenuObjects() {

        List<MenuObject> menuObjects = new ArrayList<>();

        MenuObject close = new MenuObject();
        close.setResource(R.drawable.icn_close);

        MenuObject send = new MenuObject("Configuration GPS");
        send.setResource(R.drawable.icn_1);

        MenuObject like = new MenuObject("AutoStart");
        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.icn_2);
        like.setBitmap(b);

        MenuObject addFr = new MenuObject("Coordonnees en cache");
        BitmapDrawable bd = new BitmapDrawable(getResources(),
                BitmapFactory.decodeResource(getResources(), R.drawable.icn_3));
        addFr.setDrawable(bd);

        MenuObject addFav = new MenuObject("Hide and Unhide");
        addFav.setResource(R.drawable.icn_4);

        MenuObject block = new MenuObject("Quitter");
        block.setResource(R.drawable.icn_5);

        menuObjects.add(close);
        menuObjects.add(send);
        menuObjects.add(like);
        menuObjects.add(addFr);
        menuObjects.add(addFav);
        menuObjects.add(block);
        return menuObjects;
    }

    private void initToolbar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.text_view_toolbar_title);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setNavigationIcon(R.drawable.btn_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolBarTextView.setText("Silence Tracker");
    }

    protected void addFragment(Fragment fragment, boolean addToBackStack, int containerId) {
        invalidateOptionsMenu();
        String backStackName = fragment.getClass().getName();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStackName, 0);
        if (!fragmentPopped) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(containerId, fragment, backStackName)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            if (addToBackStack)
                transaction.addToBackStack(backStackName);
            transaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.context_menu:
                if (fragmentManager.findFragmentByTag(ContextMenuDialogFragment.TAG) == null) {
                    mMenuDialogFragment.show(fragmentManager, ContextMenuDialogFragment.TAG);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mMenuDialogFragment != null && mMenuDialogFragment.isAdded()) {
            mMenuDialogFragment.dismiss();
        } else {
            finish();
        }
    }

    @Override
    public void onMenuItemClick(View clickedView, int position) {

        if (position == 1) {
            dialogtoggle();
        } else if (position == 2) {
            dialogAutostart();

        } else if (position == 3) {
            ecrireFicher("monFichier.txt", "10.124523");
            Toast.makeText(getApplicationContext(), lireFichier("monFichier.txt"), Toast.LENGTH_LONG).show();
        } else if (position == 4) {
            dialogHideAndUnhide();

        } else {
            Toast.makeText(this, "Clicked on other  position: " + position, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onMenuItemLongClick(View clickedView, int position) {
        // Toast.makeText(this, "Long clicked on position: " + position, Toast.LENGTH_SHORT).show();
        if (position == 1) {


        } else if (position == 2) {
            Toast.makeText(this, "Long Clicked on position: " + position, Toast.LENGTH_SHORT).show();

        } else if (position == 3) {
            Toast.makeText(this, "Long Clicked on position: " + position, Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, "Long Clicked on other  position: " + position, Toast.LENGTH_SHORT).show();
        }
    }

    public void dialogtoggle() {
        AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity2.this);
        LayoutInflater adbInflater = LayoutInflater.from(MainActivity2.this);
        toggleOneButton = (ToggleButton) findViewById(R.id.toggleButton1);
        toggleFiveButton = (ToggleButton) findViewById(R.id.toggleButton);
        View eulaLayout = adbInflater.inflate(R.layout.activity_alert_dialog, null);

        adb.setView(eulaLayout);
        adb.setTitle("Configuration");
        adb.setMessage(Html.fromHtml("L'envoi Des Coordonnees GPS"));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                return;
            }
        });

        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                return;
            }
        });

        adb.show();
    }

    public void optionHideAndUnhideOnclicked(View view) {
        switch (view.getId()) {
            case R.id.btnhide:
                onDisabled(getApplicationContext());
                Toast.makeText(getApplicationContext(), "hide", Toast.LENGTH_LONG).show();
                break;

        }
    }

    public void onToggleClicked(View view) {
        // Is the toggle on?
        boolean on = ((ToggleButton) view).isChecked();
        switch (view.getId()) {
            case R.id.toggleButton1:
                if (on) {

                    Intent intent = new Intent(this, BroadcastReceiverGPS.class);

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
                            this.getApplicationContext(), 234324243, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                            + (60 * 1000), pendingIntent);
                    Toast.makeText(this, "Alarm set in " + 1 + " seconds",
                            Toast.LENGTH_LONG).show();
                    startService(new Intent(this, MyService.class));

                } else {
                    // Disable vibrate
                    Toast.makeText(getApplicationContext(), "Status: off", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.toggleButton:
                if (on) {
                    Intent intent = new Intent(this, BroadcastReceiverGPS.class);

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
                            this.getApplicationContext(), 234324243, intent, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                            + (300 * 1000), pendingIntent);
                    Toast.makeText(this, "Alarm set in " + 1 + " seconds",
                            Toast.LENGTH_LONG).show();
                    startService(new Intent(this, MyService.class));

                    Toast.makeText(getApplicationContext(), "Status: on1", Toast.LENGTH_SHORT).show();
                } else {
                    // Disable vibrate
                    Toast.makeText(getApplicationContext(), "Status: off1", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                Toast.makeText(getApplicationContext(), "Status:", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public Location getLocation() {
        LocationManager locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);
        if (locationManager != null) {
            Location lastKnownLocationGPS = locationManager
                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (lastKnownLocationGPS != null) {
                return lastKnownLocationGPS;
            } else {
                Location loc = locationManager
                        .getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
                System.out.println("1::" + loc.getLongitude());
                // ----getting null over here
                System.out.println("2::" + loc.getLatitude());
                return loc;
            }
        } else {

            return null;
        }
    }

    public void dialogAutostart() {
        AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity2.this);
        LayoutInflater adbInflater = LayoutInflater.from(MainActivity2.this);
        CheckBox btnAutoStart = (CheckBox) findViewById(R.id.chkautostart);
        View eulaLayout = adbInflater.inflate(R.layout.activity_dialog_autostart, null);

        adb.setView(eulaLayout);
        adb.setTitle("Configuration");
        adb.setMessage(Html.fromHtml("Mettre votre application AutoStart"));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                return;
            }
        });

        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                return;
            }
        });

        adb.show();
    }

    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch (view.getId()) {
            case R.id.chkautostart:
                if (checked) {
                    //restart(getApplicationContext(), 1);
                    Intent i = new Intent(this, BroadcastReceiverAutostart.class);

                    PendingIntent pendingIntent = PendingIntent.getBroadcast(
                            this.getApplicationContext(), 234324243, i, 0);
                    AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
                            + (60 * 1000), pendingIntent);
                    Toast.makeText(this, "Alarm set in " + 1 + " seconds",
                            Toast.LENGTH_LONG).show();
                    startService(new Intent(this, MyService.class));


                } else {
                    Toast.makeText(getApplicationContext(), "Rien � coche", Toast.LENGTH_SHORT).show();
                    stopService(new Intent(getApplicationContext(), MyService.class));
                }

                break;

        }
    }

    private void ecrireFicher(String nomFichier, String monText) {

        File sdLien = Environment.getExternalStorageDirectory();
        File monFichier = new File(sdLien, "monFichier.txt");
        BufferedWriter writer = null;
        try {
            FileWriter out = new FileWriter(monFichier);
            writer = new BufferedWriter(out);
            writer.write(monText);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private String lireFichier(String nomFichier) {
        String monText = null;
        File sdLien = Environment.getExternalStorageDirectory();
        File monFichier = new File(sdLien + "/" + nomFichier);
        if (!monFichier.exists()) {
            throw new RuntimeException("Fichier inn�xistant dur la carte sd");
        }
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(monFichier));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            monText = builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return monText;
    }

    public void onDisabled(Context context) {
        // When the first widget is created, stop listening for the
        // TIMEZONE_CHANGED and
        // TIME_CHANGED broadcasts.
        Log.d(TAG, "onDisabled");
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(new ComponentName(this,
                        com.silence.tracker.Activity.MainActivity.class),
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

    }

    public void getUnhide() {
        PackageManager p = getPackageManager();
        ComponentName componentName = new ComponentName(this, MainActivity.class);
        p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

    }

    public void dialogHideAndUnhide() {
        AlertDialog.Builder adb = new AlertDialog.Builder(MainActivity2.this);
        LayoutInflater adbInflater = LayoutInflater.from(MainActivity2.this);
        View eulaLayout = adbInflater.inflate(R.layout.activity_hide_unhide, null);

        adb.setView(eulaLayout);
        adb.setTitle("Configuration");
        adb.setMessage(Html.fromHtml("Mettre votre application AutoStart"));
        adb.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                return;
            }
        });

        adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                return;
            }
        });

        adb.show();
    }
}


