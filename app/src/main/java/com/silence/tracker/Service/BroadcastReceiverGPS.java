package com.silence.tracker.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.silence.tracker.Utils.GPSTracker;

public class BroadcastReceiverGPS extends BroadcastReceiver {
    private String stringLatitude;
    private String stringLongitude;
    private String TAG = "BroadcastReceiverGPS";

    @Override
    public void onReceive(Context context, Intent arg1) {

        // Vibrate the mobile phone
        // Vibrator vibrator = (Vibrator)
        // context.getSystemService(Context.VIBRATOR_SERVICE);
        // vibrator.vibrate(2000);
        GPSTracker gpsTracker = new GPSTracker(context);

        if (gpsTracker.canGetLocation()) {
            stringLatitude = String.valueOf(gpsTracker.getLatitude());
            Log.v(TAG, stringLatitude);
            stringLongitude = String.valueOf(gpsTracker.getLongitude());
            Log.v(TAG, stringLongitude);

            Toast.makeText(
                    context,
                    "Latitude" + stringLatitude + "longitude" + stringLongitude,
                    Toast.LENGTH_LONG).show();
       /* } else {

            gpsTracker.showSettingsAlert();

        }*/
        }

    }
}
