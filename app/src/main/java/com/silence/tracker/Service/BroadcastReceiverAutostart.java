package com.silence.tracker.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class BroadcastReceiverAutostart extends BroadcastReceiver {
    private String stringLatitude;
    private String stringLongitude;
    private String TAG = "MyBroadcastReceiver";

    @Override
    public void onReceive(Context appContext, Intent arg1) {

        // Vibrate the mobile phone
        // Vibrator vibrator = (Vibrator)
        // context.getSystemService(Context.VIBRATOR_SERVICE);
        // vibrator.vibrate(2000);
/*
        Intent i = context. getPackageManager ()
                . getLaunchIntentForPackage ( context. getPackageName ()  );
        i . addFlags ( Intent . FLAG_ACTIVITY_CLEAR_TOP );
        startActivity ( i );*/
        Intent intentHome = appContext.getPackageManager()
                .getLaunchIntentForPackage(appContext.getPackageName());
        intentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        appContext.startActivity(intentHome);
        Toast.makeText(
                appContext,
                "AutostartService",
                Toast.LENGTH_LONG).show();


    }

}
