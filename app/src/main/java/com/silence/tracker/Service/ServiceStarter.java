package com.silence.tracker.Service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.silence.tracker.Activity.MainActivity;

public class ServiceStarter extends BroadcastReceiver {

	@Override
	public void onReceive(Context _context, Intent _intent) {

		Intent i = new Intent("com.prac.test.MyPersistingService");
		i.setClass(_context, MainActivity.class);
		_context.startService(i);
	}

}